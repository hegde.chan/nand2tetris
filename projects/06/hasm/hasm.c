//Hack platform assembler
//By Chandan Hegde (hegde.chan@gmail.com)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define A_COMMAND	0
#define C_COMMAND	1
#define L_COMMAND	2

#define MAX_COMMAND_SIZE 255

#define INSTRUCTION_SIZE 16


/* ---- PARSER MODULE ---- */
FILE *inputFile;
char currentCommand[MAX_COMMAND_SIZE];

int initParser(const char* input){
	inputFile = fopen(input, "r");
	if(inputFile == NULL){
		printf("Unable to open input file: %s\n", input);
		exit(-1);
	}
}
//The functions below may only be called if initParser() has executed successfully
int hasMoreCommands(){
	return !feof(inputFile);
}

int advance(){
	fgets(currentCommand, MAX_COMMAND_SIZE, inputFile);
	return 1;
}

int commandType(){
	int ch = currentCommand[0];
	if(ch == '@') return A_COMMAND;
	else if(ch == '(') return L_COMMAND;
	else if((ch == 'A') || (ch == 'D') || (ch == 'M')) return C_COMMAND;
	else return -1;
}

char *symbol(){
	static char sym[3];
	if(commandType() == L_COMMAND){
		sscanf(currentCommand, "(%s)", sym);
	}
	else{
		sscanf(currentCommand, "@%s", sym);
	}
	return sym;
}

char *dest(){
	static char dst[1];
	char temp[7];
	sscanf(currentCommand, "%s=%s", dst, temp);
	return dst;
}

char *comp(){
	static char cmp[3];
	char temp[1];
	sscanf(currentCommand, "%s=%s", temp, comp);
	return cmp;
}

char *jump(){
	static char jmp[3], temp[5];
	sscanf(currentCommand, "%s;%s", temp, jmp);
	return jmp;
}


/* ---- CODE MODULE ---- */
char *dst(char *mnemonic){
	static char code[3] = "000";
	if(strstr(mnemonic, "A") != NULL){
		code[0] = '1';
	}
	if(strstr(mnemonic, "D") != NULL){
		code[1] = '1';
	}
	if(strstr(mnemonic, "M") != NULL){
		code[2] = '1';
	}
	return code;
}

char *cmp(char *mnemonic){
	if(strcmp(mnemonic, "0") == 0){
		return "0101010";
	}
	else if(strcmp(mnemonic, "1") == 0){
		return "0111111";
	}
	else if(strcmp(mnemonic, "-1") == 0){
		return "0111010";
	}
	else if(strcmp(mnemonic, "D") == 0){
		return "0001100";
	}
	else if(strcmp(mnemonic, "A") == 0){
		return "0110000";
	}
	else if(strcmp(mnemonic, "!D") == 0){
		return "0001101";
	}
	else if(strcmp(mnemonic, "!A") == 0){
		return "0110001";
	}
	else if(strcmp(mnemonic, "-D") == 0){
		return "0001111";
	}
	else if(strcmp(mnemonic, "-A" == 0)){
		return "0110011";
	}
	else if(strcmp(mnemonic, "D+1") == 0){
		return "0011111";
	}
	else if(strcmp(mnemonic, "A+1") == 0){
		return "0110111";
	}
	else if(strcmp(mnemonic, "D-1") == 0){
		return "0001110";
	}
	else if(strcmp(mnemonic, "A-1") == 0){
		return "0110010";
	}
	else if(strcmp(mnemonic, "D+A") == 0){
		return "0000010";
	}
	else if(strcmp(mnemonic, "D-A") == 0){
		return "0010011";
	}
	else if(strcmp(mnemonic, "A-D") == 0){
		return "0000111";
	}
	else if(strcmp(mnemonic, "D&A") == 0){
		return "0000000";
	}
	else if(strcmp(mnemonic, "D|A") == 0){
		return "0010101";
	}
	else if(strcmp(mnemonic, "M") == 0){
		return "1110000";
	}
	else if(strcmp(mnemonic, "!M") == 0){
		return "1110001";
	}
	else if(strcmp(mnemonic, "-M") == 0){
		return "1110011";
	}
	else if(strcmp(mnemonic, "M+1") == 0){
		return "1110111";
	}
	else if(strcmp(mnemonic, "M-1") == 0){
		return "1110010";
	}
	else if(strcmp(mnemonic, "D+M") == 0){
		return "1000010";
	}
	else if(strcmp(mnemonic, "D-M") == 0){
		return "1010011";
	}
	else if(strcmp(mnemonic, "M-D") == 0){
		return "1000111";
	}	
	else if(strcmp(mnemonic, "D&M") == 0){
		return "10000000";
	}
	else if(strcmp(mnemonic, "D|M") == 0){
		return "1010101";
	}
}

char *jmp(char *mnemonic){
	if(strcmp(mnemonic, "JGT") == 0){
		return "001";
	}
	else if(strcmp(mnemonic, "JEQ") == 0){
		return "010";
	}
	else if(strcmp(mnemonic, "JGE") == 0){
		return "011";
	}
	else if(strcmp(mnemonic, "JLT") == 0){
		return "100";
	}
	else if(strcmp(mnemonic, "JNE") == 0){
		return "101";
	}
	else if(strcmp(mnemonic, "JLE") == 0){
		return "110";
	}
	else if(strcmp(mnemonic, "JMP") == 0){
		return "111";
	}
}

/* ---- MAIN ---- */
int main(int argc, char **argv){
	char *filename = argv[1];
	FILE *outputFile = fopen("out.hack", "w");
	if(outputFile == NULL){
		printf("Unable to create output\n");
		return -1;
	}
	initParser(filename);
	while(hasMoreCommands()){
		advance();
		char *mnemonic = "";
		switch(commandType()){
			case A_COMMAND:
			case L_COMMAND:
				fwrite()
				break;
			case C_COMMAND:
				strcat(mnemonic, comp());
				strcat()
		}
	}
	return 0;
}
