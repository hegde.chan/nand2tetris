#ifndef _PARSER_H
#define _PARSER_H
#include <stdio.h>

#define A_COMMAND 0
#define C_COMMAND 1
#define L_COMMAND 2

int initParser(FILE *input);

int hasMoreCommands();

int advance();

int

#endif